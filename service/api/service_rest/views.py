from django.shortcuts import render
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from common.json import ModelEncoder
from .models import AutomobileVO, Appointment, Technician

# Create your views here.

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_number",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "date",
        "technician",
        "reason",
        "completed",
        "vip",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all() #gets all techs
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)  #creats a tech
        technicians = Technician.objects.create(**content)
        return JsonResponse(
            technicians,
            encoder=TechnicianEncoder,
            safe=False,
        )
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_technicians(request, id):
    if request.method == "GET":  # get one tech by id 
        try:
            technicians = Technician.objects.get(id=id)
            return JsonResponse(
                technicians,
                TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:  #if tech doesnt exist return error
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=400,
            )
    elif request.method=="DELETE":    #delete method by id
        try:
            count,_ = Technician.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Technician.DoesNotExist:    #error message if tech doesnt exist
            return JsonResponse(
                {"message": "Cant delete Technician does not exist"},
                status=400,
            )
    else:
        try:                                        #update method by id 
            content = json.loads(request.body)
            Technician.objects.filter(id=id).update(**content)
            technicians = Technician.objects.get(id=id)
            return JsonResponse(
                technicians,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:     #error message if tech doenst exist
            return JsonResponse(
                {"message": "Cant update Technician does not exist"},
                status=400,
            )

@require_http_methods(["GET","POST"])
def api_list_appointments(request):
    if request.method == "GET":         #gets all appointments 
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder= AppointmentEncoder,
            safe = False
        )
    else:
        content = json.loads(request.body)
        try:
            technicians = Technician.objects.get(id=content["technician"])
            content["technician"] = technicians
            try:
                if AutomobileVO.objects.get(vin=content["vin"]):
                    content["vip"] =True
            except AutomobileVO.DoesNotExist:
                content["vip"]=False
        except Technician.DoesNotExist:   
            return JsonResponse(
                        {"message": "auto does not exist"},
                        status=400,
                )
        try:
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe = False
            )
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)


@require_http_methods(["GET", "PUT", "DELETE"])       
def api_show_appointments(request, id):
    if request.method =="GET":
        try:
            appointments = Appointment.objects.get(id=id)
            return JsonResponse(
                appointments,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:   
            return JsonResponse(
                        {"message": "Appointment does not exist"},
                        status=400,
                )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            Appointment.objects.filter(id=id).update(**content)
            appointments = Appointment.objects.get(id=id)
            return JsonResponse(
                appointments,
                encoder= AppointmentEncoder,
                safe=False,

            )
        except Appointment.DoesNotExist:   
            return JsonResponse(
                        {"message": "Can't update Appointment does not exist"},
                        status=400,
                )
    else:
        try:
            count, _ = Appointment.objects.filter(id=id).delete()
            return JsonResponse({'deleted': count>0})
        except Appointment.DoesNotExist:   
            return JsonResponse(
                        {"message": "Can't delete because appt does not exist"},
                        status=400,
                )