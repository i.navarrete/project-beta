from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    color = models.CharField(max_length=50 )
    year = models.IntegerField(default=0000)
    vin = models.CharField(max_length=20, unique=True)
    def __str__(self):
        return self.vin

class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name


class Appointment(models.Model):
    vin = models.CharField(max_length=100)
    customer_name = models.CharField(max_length=100)
    date = models.DateTimeField(null=True)

    technician = models.ForeignKey(
        Technician,
        related_name="service", 
        on_delete=models.CASCADE,
    )

    reason = models.TextField()
    vip = models.BooleanField(default=False, null=True, blank=True)
    completed = models.BooleanField(default=False, null=True, blank=True)
    
    def __str__(self):
        return self.vin


