from django.urls import path
from .views import api_sales_persons, api_sales_person, api_sales_customers, api_sales_customer, api_list_salesrecords, api_sales_record_details


urlpatterns = [
    path("salesrecords/<int:pk>/", api_sales_record_details, name="api_sales_record_details"),
    path("salesrecords/", api_list_salesrecords, name="api_list_salesrecords"),
    path("salescustomer/<int:pk>/", api_sales_customer, name="api_sales_customer"),
    path("salescustomers/", api_sales_customers, name="api_sales_customers"),
    path("salespersons/<int:pk>/", api_sales_person, name="api_sales_person"),
    path("salespersons/", api_sales_persons, name="api_sales_persons"),
]
