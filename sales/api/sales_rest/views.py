from django.shortcuts import render
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import (
    SalesPerson,
    SalesCustomer,
    AutomobileVO,
    SalesRecord,
)


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number",
    ]

class SalesCustomerEncoder(ModelEncoder):
    model = SalesCustomer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "year",
        "color",
        "vin",
        "available",
    ]

class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "sales_person",
        "automobile",
        "sales_customer",
        "price",
        "id"
    ]
    def get_extra_data(self, o):
        return {
            "automobile": o.automobile.vin,
            "sales_person": {
                 "name" :o.sales_person.name,
                 "emp_no":o.sales_person.employee_number,
            },
            "sales_customer": o.sales_customer.name
        }

    encoders = {
        "sales_person": SalesPersonEncoder(),
        "sales_customer": SalesCustomerEncoder(),
    }




@require_http_methods(["GET", "POST"])
def api_sales_persons(request):
    if request.method == "GET":
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": sales_persons},
            encoder=SalesPersonEncoder,
        )
    else: #POST
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Unable to create Sales Person"},
                status=400,
                )
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_sales_person(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message": "Salesperson does not exist"},
                status=404,
                )
            return response
    elif request.method == "DELETE":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            sales_person.delete()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
    else: #PUT
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(id=pk)
            props = ["name", "employee_number"]
            for prop in props:
                if prop in content:
                    setattr(sales_person, prop, content[prop])
            sales_person.save()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message": "Salesperson does not exist"},
                status=404,
                )
            return response

@require_http_methods(["GET", "POST"])
def api_sales_customers(request):
    if request.method == "GET":
        sales_customers = SalesCustomer.objects.all()
        return JsonResponse(
            {"sales_customers": sales_customers},
            encoder=SalesCustomerEncoder,
        )
    else: #POST
        try:
            content = json.loads(request.body)
            sales_customer = SalesCustomer.objects.create(**content)
            return JsonResponse(
                sales_customer,
                encoder=SalesCustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Unable to create Sales customer"},
                status=400,
            )
            return response

@require_http_methods(["GET", "DELETE", "PUT"])
def api_sales_customer(request, pk):
    if request.method == "GET":
        try:
            sales_customer = SalesCustomer.objects.get(id=pk)
            return JsonResponse(
                sales_customer,
                encoder=SalesCustomerEncoder,
                safe=False,
            )
        except SalesCustomer.DoesNotExist:
            response = JsonResponse(
                {"message": "Sales customer does not exist"},
                status=404,
            )
            return response
    elif request.method == "DELETE":
        try:
            sales_customer = SalesCustomer.objects.get(id=pk)
            sales_customer.delete()
            return JsonResponse(
                sales_customer,
                encoder=SalesCustomerEncoder,
                safe=False,
            )
        except SalesCustomer.DoesNotExist:
            return JsonResponse(
                {"message": "Sales customer does not exist"}
            )
    else: #PUT
        try:
            content = json.loads(request.body)
            sales_customer = SalesCustomer.objects.get(id=pk)

            props = ["name", "address", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(sales_customer, prop, content[prop])
            sales_customer.save()
            return JsonResponse(
                sales_customer,
                encoder=SalesCustomerEncoder,
                safe=False,
            )
        except SalesCustomer.DoesNotExist:
            response = JsonResponse(
                {"message": "Sales customer does not exist"},
                status=404,
            )
            return response

@require_http_methods(["GET", "POST"])
def api_list_salesrecords(request):
    if request.method == "GET":
        sales_records = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_records": sales_records},
            encoder=SalesRecordEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:

            auto_id = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=auto_id)
            content["automobile"] = automobile

            sales_person_name = content["sales_person"]
            sales_person = SalesPerson.objects.get(name=sales_person_name)
            content["sales_person"] = sales_person

            sales_customer_name = content["sales_customer"]
            sales_customer = SalesCustomer.objects.get(name=sales_customer_name)
            content["sales_customer"] = sales_customer

            sales_record = SalesRecord.objects.create(**content)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            response = JsonResponse(
                {"message": "Unable to create Sales record"},
                status=400,
            )
            return response

@require_http_methods(["GET", "DELETE"])
def api_sales_record_details(request, pk):
    if request.method == "GET":
        try:
            sales_record = SalesRecord.objects.get(id=pk)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse(
                {"message": "Record does not exist"},
                status=404,
            )
            return response
    else: #DELETE
        try:
            sales_record = SalesRecord.objects.get(id=pk)
            sales_record.delete()
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse({"message": "Record does not exist"})
