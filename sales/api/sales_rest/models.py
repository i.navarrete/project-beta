from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=500, unique=True)
    year = models.PositiveSmallIntegerField()
    color = models.CharField(max_length=50)
    vin = models.CharField(max_length=17, unique=True)
    available = models.BooleanField(default=True)

    def __str__(self):
        return self.vin

class SalesPerson(models.Model):
    name = models.CharField(max_length=50, unique=True)
    employee_number = models.PositiveIntegerField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_sales_person", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("name",)

class SalesCustomer(models.Model):
    name = models.CharField(max_length=50, unique=True)
    address = models.CharField(max_length=200)
    phone_number = models.PositiveBigIntegerField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_sales_customer", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("name",)

class SalesRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales_records",
        on_delete=models.PROTECT,
    )

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales_records",
        on_delete=models.PROTECT,
    )

    sales_customer = models.ForeignKey(
        SalesCustomer,
        related_name="sales_records",
        on_delete=models.PROTECT,
    )

    price = models.CharField(max_length=50)
    created = models.DateTimeField(auto_now_add=True)

    def get_api_url(self):
        return reverse("api_sales_record", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("created",)
