import React, {useEffect, useState} from 'react';

function CreateModel(props) {
    const [name, setName] = useState("")
    const [pictureURL, setPicture] = useState("")
    const [manufacturer, setManufacturer] = useState("")

    const handleChangeName = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleChangePicture = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.picture_url = pictureURL;
        data.manufacturer_id = manufacturer;

        const vehicleURL = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(vehicleURL, fetchConfig);
        if (response.ok) {
            const newVehicle = await response.json();
            console.log(newVehicle);
            setName("");
            setPicture("");
            setManufacturer("");
            props.getModels();
        }
    }

    const [manufacturers, setManufacturers] = useState([])
    const fetchData = async () => {
    const ManufacturersURL = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(ManufacturersURL);

    if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers)
        }
    }
    useEffect(() => {
        fetchData();
     }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a vehicle model</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <input onChange={handleChangeName} value={name} placeholder="vehicle name" required type="text" name="vehicleName" id="vehicleName" className="form-control" />
                  <label htmlFor="vehicleNameChange">Vehicle Name</label>
                </div>
                  <div className="form-floating mb-3">
                    <input onChange={handleChangePicture} value={pictureURL} placeholder="Vehicle picture" required name="vehiclePicture" id="vehicle_picture" className="form-control" />
                    <label htmlFor="vehiclePicture">Vehicle Image URL (max characters:200)</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={handleChangeManufacturer} value={manufacturer} required name="manufacturers" id="ManList" className="form-select">
                        <option value="">Choose Manufacturer</option>
                        {manufacturers.map(manufacturer => {
                            return (
                                <option key={manufacturer.id} value={manufacturer.id}>
                                    {manufacturer.name}
                                </option>
                                );
                        })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>



     );
}

export default CreateModel;
