import React, {useEffect, useState} from 'react'

function SalesPersonForm(props) {
    const [name, setName] = useState("");
    const handleChangeName = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [employeeNumber, setEmployeeNumber] = useState("");
    const handleChangeEmployeeNumber = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.employee_number = employeeNumber;

        const salesPersonURL = "http://localhost:8090/api/salespersons/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesPersonURL, fetchConfig);
        if (response.ok) {
            const newSalesPerson = await response.json();
            console.log(newSalesPerson)
            setName("");
            setEmployeeNumber("");
            props.getSalesPerson()
        }
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Salesperson</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input value={name} onChange={handleChangeName} placeholder="Fabric" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={employeeNumber} onChange={handleChangeEmployeeNumber} placeholder="Fabric" required type="text" name="employee_number" id="employee_number" className="form-control"/>
                <label htmlFor="employee_number">Employee Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

    )
}
export default SalesPersonForm
