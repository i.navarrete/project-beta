import React, { useState, useEffect } from "react"

function AppointmentForm() {
    const [vin, setVin] = useState('');
    const [customerName, setCustomerName] = useState('');
    const [date, setDate] = useState('');
    const [techName, setTechName] = useState('');
    const [technicians, setTechnicians] = useState([]);
    const [reason, setReason] = useState('');
    const [vip, setVip] = useState('');
    

    const fetchData = async () => {
        const URL = "http://localhost:8080/api/technicians/";
        const response = await fetch(URL);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.vin = vin;
        data.customer_name = customerName;
        data.date = date;
        data.technician = techName;
        data.reason = reason;
        data.vip = vip;

        const appointmentUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();

            setVin('');
            setCustomerName('');
            setDate('');
            setTechName('');
            setReason('');
            setVip('');

        }
    }
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerNameChange = (event) => {
        const value = event.target.value;
        setCustomerName(value);
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTechNameChange = (event) => {
        const value = event.target.value;
        setTechName(value);
    }
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }
    const handleVipChange = (event) => {
        const value = event.target.value;
        setVip(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create an appointment</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="Vin Name" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">Vin</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerNameChange} value={customerName} placeholder="Customer name" required type="text" name="customerName" id="customerName" className="form-control" />
                            <label htmlFor="customerName">Customer name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateChange} value={date} placeholder="date" required type="text" name="date" id="date" className="form-control" />
                            <label htmlFor="vin">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleTechNameChange} value={techName} placeholder="Tech name" required id="technician" name="technician" className="form-select">
                                <option value="">Choose a tech </option>
                                {technicians.map(techName => {
                                    return (
                                        <option key={techName.id} value={techName.id}> {techName.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleVipChange} value={vip} placeholder="VIP" required id="vip" name="vip" className="form-select">
                                <option value="">Is customer a VIP ?</option>
                                <option value="True">True </option>
                                <option value="False">False </option>
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );

}

export default AppointmentForm;