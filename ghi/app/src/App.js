import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ListSales from './ListSales';
import ListManufacturer from './ListManufacturer'
import SalesPersonForm from './SalesPersonForm';
import SalesCustomerForm from './SalesCustomerForm';
import SalesRecordForm from './SalesRecordForm';
import SelectSalesRecord from './SelectSalesRecords';
import ManufacturerForm from './ManufacturerForm';
import CreateModel from './ModelsForm';
import ListModels from './ListModels';
import ListAutomobiles from './ListAutomobiles';
import CreateAutomobile from './AutomobileForm';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import React, {useEffect, useState} from 'react';


function App() {
  const [manufacturers, setManufacturers] = useState([]);
  const getManufacturers = async () => {
    const URL = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(URL);
    if (response.ok) {
      const data = await response.json()
      setManufacturers(data.manufacturers)
    }
  }

  const [models, setModels] = useState([]);
  const getModels = async () => {
    const URL = 'http://localhost:8100/api/models/';
    const response = await fetch(URL);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models)
    }
  }

  const [customer, setCustomer] = useState([]);
  const getCustomer = async () => {
    const URL = 'http://localhost:8090/api/salescustomers/';
    const response = await fetch(URL);
    if (response.ok) {
      const data = await response.json();
      setCustomer(data.customers)
    }
  }

  const [salesperson, setSalesPersons] = useState([]);
  const getSalesPerson = async () => {
    const URL = 'http://localhost:8090/api/salespersons/';
    const response = await fetch(URL);
    if (response.ok) {
      const data = await response.json();
      setSalesPersons(data.salespersons)
    }
  }

  const [automobile, setAutomobile] = useState([]);
  const getAutomobile = async () => {
    const URL = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(URL);
    if (response.ok) {
      const data = await response.json();
      setAutomobile(data.autos)
    }
  }

  const [sales, setSales] = useState([]);
  const getSales = async () => {
    const URL = "http://localhost:8090/api/salesrecords/";
    const response = await fetch(URL);
    if (response.ok) {
      const data = await response.json();
      setSales(data.salesrecords)
    }
  }

  useEffect(() => {
    getManufacturers();
    getModels();
    getCustomer();
    getAutomobile();
    getSalesPerson();
    getSales();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" >
            <Route path="" element={<ListManufacturer manufacturers={manufacturers} getManufacturers={getManufacturers} /> } />
            <Route path="new" element={<ManufacturerForm manufacturers={manufacturers} getManufacturers={getManufacturers} />}/>
          </Route>
          <Route path="models" >
            <Route path="" element={<ListModels models={models} getModels={getModels} /> } />
            <Route path="new" element={<CreateModel models={models} getModels={getModels} />}/>
          </Route>
          <Route path="automobiles" >
            <Route path="" element={<ListAutomobiles automobile={automobile} getAutomobile={getAutomobile} /> } />
            <Route path="new" element={<CreateAutomobile automobile={automobile} getAutomobile={getAutomobile} />}/>
          </Route>
          <Route path="salesperson/">
            <Route path="new" element={<SalesPersonForm salesperson={salesperson} getSalesPerson={getSalesPerson} />}/>
            <Route path="" element={<SelectSalesRecord salesperson={salesperson} getSalesPerson={getSalesPerson}/>} />
          </Route>
          <Route path="salescustomers/new" element={<SalesCustomerForm customer={customer} getCustomer={getCustomer} />} />
          <Route path="sales/new" element={<SalesRecordForm sales={sales} getSales={getSales} />} />
          <Route path="sales/">
          <Route path="" element={<ListSales sales={sales}/>}/>

          </Route>
          <Route path="technicians/">
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments/">
            <Route path="new" element={<AppointmentForm />} />
            <Route path="" element={<AppointmentList />} />
            <Route path="/appointments/all" element={<ServiceHistory />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
