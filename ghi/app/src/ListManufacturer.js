import React, {useEffect, useState} from 'react'

function ListManufacturer(props) {

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                {props.manufacturers.map(manufacturer => {
                return (
                    <tr key={manufacturer.name}>
                        <td>{ manufacturer.name }</td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
    );
  }

export default ListManufacturer;
