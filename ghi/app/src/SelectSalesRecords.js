import React, {useState, useEffect} from 'react';

function SelectSalesRecord() {
    const [sales_persons, setSalesPerson] = useState([]);
    const [selected_person, setSelectedPerson] = useState('')
    const [salesRecord, setRecords] = useState([]);
    async function getRecords() {
        const response = await fetch("http://localhost:8090/api/salesrecords/");
        if (response.ok) {
            let data = await response.json();
            setRecords(data.sales_records);
        }
    }

    const handlePersonChange = (event) => {
        const value = event.target.value
        setSelectedPerson(value)
        console.log(value)
    }

    useEffect(() => {
        const salesRecordsURL = 'http://localhost:8090/api/salesrecords/';
        const salesPersonsURL = 'http://localhost:8090/api/salespersons/';
        const URLs = [salesRecordsURL, salesPersonsURL]
        Promise.all(URLs.map(URL => fetch(URL)
            .then(response => response.json())
        ))
            .then((data) => {
                setRecords(data[0].sales_records);
                setSalesPerson(data[1].sales_persons);
            })
            .catch(e => console.error('ERROR: ', e))

    }, [])

    let tableClass = 'table table-striped d-none'
    if (selected_person !== '') {
        tableClass = 'table table-striped'
    }

    return (
        <div>
            <div className="shadow p-4 mt-4">
                <h1>Sales Person History</h1>
                <select onChange={handlePersonChange} required
                    name="sales_persons" id="sales_persons"
                    className="form-select" value={selected_person} >
                    <option value="">Choose a Sales Person</option>
                    {sales_persons.map(sP => {
                        return (
                            <option key={sP.id} value={sP.name}>
                                {sP.name}
                            </option>
                        );
                    })}
                </select>
                <table className={tableClass}>
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Sales Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    {salesRecord.map(sales => {
                            if (sales.sales_person.name === selected_person) {
                                return (
                                    <tr key={sales.id}>
                                        <td>{sales.sales_person.name}</td>
                                        <td>{sales.sales_customer}</td>
                                        <td>{sales.automobile}</td>
                                        <td>${sales.price}</td>
                                    </tr>
                                )
                            }
                     } )}
                    </tbody>
                </table>
            </div>
        </div>

    )
}
export default SelectSalesRecord
