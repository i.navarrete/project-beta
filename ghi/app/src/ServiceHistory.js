import React, { useEffect, useState, } from "react";

function ServiceHistory() {
    const [vin, setVin] = useState('');
    const [appointments, setAppt] = useState([]);
    
    
    const fetchData = async () => {
        const URL = "http://localhost:8080/api/appointments/";
        const response = await fetch(URL);
        if (response.ok) {
            const data = await response.json();
            setAppt(data.appointments);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleVinChange = (event) => {
            const value = event.target.value;
            setVin(value);
        }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = appointments.filter((appointment) =>
            appointment.vin.includes(vin)
        )
        setAppt(data);

    }

        return (
            <>
              <div className="container p-0">
                <div className="row" >
                  <div className="col d-flex justify-content-center align-items-center">
                  <h2 className="my-3">Service appointments</h2>
                  </div>
                  <div className="row">
                  <form onSubmit={handleSubmit} id="appointments-history-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
                        <label htmlFor="vin">Enter VIN Number</label>
                    </div>
                    <button className="btn btn-primary">Search</button>
                  </form>
                  </div>
                  <div className="col-sm-auto d-flex justify-content-center align-items-center">
                  </div>
                </div>
              </div>
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th>VIN</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Reason</th>
                    <th>Technician</th>
                  </tr>
                </thead>
                <tbody>
                  {appointments.map((appt) => {
                    if (vin === appt.vin) {
                      return (
                        <tr key={appt.id}>
                            <td>{appt.vin}</td>
                            <td>{appt.customer_name}</td>
                            <td>{appt.reason}</td>
                            <td>{appt.technician.name}</td>
                        </tr>
                      );
                    }
                  })}
                </tbody>
              </table>
            </>
          );
        }
      
      export default ServiceHistory;
      