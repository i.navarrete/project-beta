import React, {useEffect, useState} from 'react';

function CreateAutomobile(props) {
    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')
    const [model, setModel] = useState('')
    const [models, setModels] = useState([])

    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleChangeYear = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const handleChangeVin = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleChangeModel = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}

        data.color = color
        data.year = year
        data.vin = vin
        data.model_id = model

        const automobileURL = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(automobileURL, fetchConfig)
        if (response.ok) {
            const newAutomobile = await response.json()
            console.log(newAutomobile)
            setColor('')
            setYear('')
            setVin('')
            setModel('')
            props.getAutomobile()
        }
    }

    const fetchData = async () => {
        const URL = 'http://localhost:8100/api/models/';
        const response = await fetch(URL);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }
    }
    useEffect(() => {
        fetchData();
        }, []);
return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add new automobile</h1>
            <form onSubmit={handleSubmit} id="create-automobiles-form">
              <div className="form-floating mb-3">
                <input value={color} onChange={handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={year} onChange={handleChangeYear} placeholder="Year" required type="number" name="year" id="year" className="form-control" />
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input value={vin} onChange={handleChangeVin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="mb-3">
                    <select value={model} onChange={handleChangeModel} required id="model" name="model" className="form-select">
                        <option value=''>Choose a model</option>
                        {models.map(model => {
                                return (
                                    <option key={model.id} value={model.id}>
                                        {model.name}
                                    </option>
                                )
                        ;
                        })}
                    </select>
                </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

)
}
export default CreateAutomobile;
